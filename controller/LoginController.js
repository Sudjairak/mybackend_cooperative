const Student = require('../model/Student')
const loginController = {
  addStudent (req, res, next) {
    const payload = req.body
    Student.create(payload).then(function (students) {
      res.json(students)
    }).catch(function (err) {
      res.status(500).send(err)
    })
  },
  getStudent (req, res, next) {
    const { id } = req.params
    Student.find({ email: id }).then(function (students) {
      res.json(students)
    }).catch(function (err) {
      res.status(500).send(err)
    })
  },
  signin (req, res, next) {
    const myemail = req.body.email
    const mypasswd = req.body.passwd
    console.log('myemail: ' + myemail)
    Student.find({ email: myemail, passwd: mypasswd }).then(function (students) {
      if (typeof students[0] === 'undefined') {
        res.status(500).send('username or password invalid')
      } else {
        res.json(students[0])
        console.log(students[0])
      }
    }).catch(function (err) {
      res.status(500).send(err)
    })
  }
}

module.exports = loginController
