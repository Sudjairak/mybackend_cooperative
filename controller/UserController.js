const User = require('../model/User')
const usersController = {
  getUsers (req, res, next) {
    User.find({}).then(function (students) {
      console.log(students)
      res.json(students)
    }).catch(function (err) {
      console.log(err)
      res.status(500).send(err)
    })
  },
  updateUser (req, res, next) {
    const payload = req.body
    // res.json(usersController.updateStudent(payload))
    User.updateOne({ _id: payload._id }, payload).then(function (users) {
      res.json(users)
    }).catch(function (err) {
      res.status(500).send(err)
    })
  },
  getUser (req, res, next) {
    const { id } = req.params
    User.findById(id).then(function (users) {
      res.json(users)
    }).catch(function (err) {
      res.status(500).send(err)
    })
  }
}

module.exports = usersController
