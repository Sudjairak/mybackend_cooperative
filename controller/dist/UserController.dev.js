"use strict";

var User = require('../model/User');

var usersController = {
  getUsers: function getUsers(req, res, next) {
    User.find({}).then(function (students) {
      console.log(students);
      res.json(students);
    })["catch"](function (err) {
      console.log(err);
      res.status(500).send(err);
    });
  },
  updateUser: function updateUser(req, res, next) {
    var payload = req.body; // res.json(usersController.updateStudent(payload))

    User.updateOne({
      _id: payload._id
    }, payload).then(function (users) {
      res.json(users);
    })["catch"](function (err) {
      res.status(500).send(err);
    });
  },
  getUser: function getUser(req, res, next) {
    var id = req.params.id;
    User.findById(id).then(function (users) {
      res.json(users);
    })["catch"](function (err) {
      res.status(500).send(err);
    });
  }
};
module.exports = usersController;