"use strict";

var Student = require('../model/Student');

var studentsController = {
  addStudent: function addStudent(req, res, next) {
    var payload = req.body;
    Student.create(payload).then(function (students) {
      res.json(students);
    })["catch"](function (err) {
      res.status(500).send(err);
    });
  },
  updateStudent: function updateStudent(req, res, next) {
    var payload = req.body; // res.json(studentsController.updateStudent(payload))

    Student.updateOne({
      _id: payload._id
    }, payload).then(function (students) {
      res.json(students);
    })["catch"](function (err) {
      res.status(500).send(err);
    });
  },
  deleteStudent: function deleteStudent(req, res, next) {
    var id, students;
    return regeneratorRuntime.async(function deleteStudent$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            id = req.params.id;
            _context.prev = 1;
            _context.next = 4;
            return regeneratorRuntime.awrap(Student.deleteOne({
              _id: id
            }));

          case 4:
            students = _context.sent;
            res.json(students);
            _context.next = 11;
            break;

          case 8:
            _context.prev = 8;
            _context.t0 = _context["catch"](1);
            res.status(500).send(_context.t0);

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, null, null, [[1, 8]]);
  },
  getStudents: function getStudents(req, res, next) {
    Student.find({}).then(function (students) {
      console.log(students);
      res.json(students);
    })["catch"](function (err) {
      console.log(err);
      res.status(500).send(err);
    });
  },
  getStudent: function getStudent(req, res, next) {
    var id = req.params.id;
    Student.findById(id).then(function (students) {
      res.json(students);
    })["catch"](function (err) {
      res.status(500).send(err);
    });
  } // ,signin (req, res, next) {
  //   const { email } = req.body.email
  //   const { passwd } = req.body.passwd
  //   Student.find(email).then(function (students) {
  //     const mypasswd = students[0].passwd
  //     if (passwd === mypasswd) {
  //       res.json(students)
  //     }
  //   })
  // }

};
module.exports = studentsController;