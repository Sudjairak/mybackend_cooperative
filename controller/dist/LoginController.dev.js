"use strict";

var Student = require('../model/Student');

var loginController = {
  addStudent: function addStudent(req, res, next) {
    var payload = req.body;
    Student.create(payload).then(function (students) {
      res.json(students);
    })["catch"](function (err) {
      res.status(500).send(err);
    });
  },
  getStudent: function getStudent(req, res, next) {
    var id = req.params.id;
    Student.find({
      email: id
    }).then(function (students) {
      res.json(students);
    })["catch"](function (err) {
      res.status(500).send(err);
    });
  },
  signin: function signin(req, res, next) {
    var myemail = req.body.email;
    var mypasswd = req.body.passwd;
    console.log('myemail: ' + myemail);
    Student.find({
      email: myemail,
      passwd: mypasswd
    }).then(function (students) {
      if (typeof students[0] === 'undefined') {
        res.status(500).send('username or password invalid');
      } else {
        res.json(students[0]);
        console.log(students[0]);
      }
    })["catch"](function (err) {
      res.status(500).send(err);
    });
  }
};
module.exports = loginController;