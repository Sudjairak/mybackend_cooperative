var express = require('express')
var router = express.Router()
const usersController = require('../controller/UserController')

/* GET users listing. */
router.get('/', usersController.getUsers)

router.get('/:id', usersController.getUser)

router.put('/', usersController.updateUser)

module.exports = router
