"use strict";

var express = require('express');

var router = express.Router();

var loginController = require('../controller/LoginController');
/* GET users listing. */
// router.get('/', studentsController.getStudents)


router.get('/:id', loginController.getStudent); // router.get('/:name', studentsController.getStudentName)

router.post('/', loginController.signin); // router.put('/', studentsController.updateStudent)
// router.delete('/:id', studentsController.deleteStudent)

module.exports = router;