"use strict";

var express = require('express');

var path = require('path');

var cookieParser = require('cookie-parser');

var logger = require('morgan');

var cors = require('cors');

var config = require('./config/db'); // var indexRouter = require('./routes/index')


var usersRouter = require('./routes/users');

var studentsRouter = require('./routes/students');

var loginRouter = require('./routes/login');

var app = express();

var mongoose = require('mongoose');

mongoose.set('useCreateIndex', true);
mongoose.connect(config.database, {
  useUnifiedTopology: true,
  useNewUrlParser: true
}); // mongoose.connect('mongodb://127.0.0.1:27017/', { useNewUrlParser: true })

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connect error!'));
db.once('open', function () {
  console.log('connect to mongo atlas');
});
app.use(logger('dev'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express["static"](path.join(__dirname, 'public'))); // app.use('/', indexRouter)

app.use('/users', usersRouter);
app.use('/students', studentsRouter);
app.use('/login', loginRouter);
module.exports = app;