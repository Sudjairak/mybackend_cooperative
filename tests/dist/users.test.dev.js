"use strict";

var dbHandler = require('./db-handler');

var Student = require('../model/Student');
/**
 * Connect to a new in-memory database before running any tests.
 */


beforeAll(function _callee() {
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap(dbHandler.connect());

        case 2:
        case "end":
          return _context.stop();
      }
    }
  });
});
/**
 * Clear all test data after every test.
 */

afterEach(function _callee2() {
  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap(dbHandler.clearDatabase());

        case 2:
        case "end":
          return _context2.stop();
      }
    }
  });
});
/**
 * Remove and close the db and server.
 */

afterAll(function _callee3() {
  return regeneratorRuntime.async(function _callee3$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return regeneratorRuntime.awrap(dbHandler.closeDatabase());

        case 2:
        case "end":
          return _context3.stop();
      }
    }
  });
});
/**
 * Product test suite.
 */

describe('Student', function () {
  it('สามารถเพิ่ม ข้อมูล ได้', function _callee4() {
    var err, user;
    return regeneratorRuntime.async(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            err = null;
            _context4.prev = 1;
            user = new Student(userComplete);
            _context4.next = 5;
            return regeneratorRuntime.awrap(user.save());

          case 5:
            _context4.next = 10;
            break;

          case 7:
            _context4.prev = 7;
            _context4.t0 = _context4["catch"](1);
            err = _context4.t0;

          case 10:
            expect(err).toBeNull();

          case 11:
          case "end":
            return _context4.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่ม ข้อมูล ได้ เพราะ ห้ามมีช่องไหนเป็น ช่องว่าง', function _callee5() {
    var err, user;
    return regeneratorRuntime.async(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            err = null;
            _context5.prev = 1;
            user = new Student(userError1);
            _context5.next = 5;
            return regeneratorRuntime.awrap(user.save());

          case 5:
            _context5.next = 10;
            break;

          case 7:
            _context5.prev = 7;
            _context5.t0 = _context5["catch"](1);
            err = _context5.t0;

          case 10:
            expect(err).not.toBeNull();

          case 11:
          case "end":
            return _context5.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่ม ข้อมูล ได้ เพราะ รหัสนิสิตต้องเท่ากับ 8 ตัว', function _callee6() {
    var err, user;
    return regeneratorRuntime.async(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            err = null;
            _context6.prev = 1;
            user = new Student(userErrorId);
            _context6.next = 5;
            return regeneratorRuntime.awrap(user.save());

          case 5:
            _context6.next = 10;
            break;

          case 7:
            _context6.prev = 7;
            _context6.t0 = _context6["catch"](1);
            err = _context6.t0;

          case 10:
            expect(err).not.toBeNull();

          case 11:
          case "end":
            return _context6.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่ม ข้อมูล รหัสนิสิตไม่สามารถซ้ำกันได้', function _callee7() {
    var err, user1, user2;
    return regeneratorRuntime.async(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            err = null;
            _context7.prev = 1;
            user1 = new Student(userComplete);
            _context7.next = 5;
            return regeneratorRuntime.awrap(user1.save());

          case 5:
            user2 = new Student(userComplete2);
            _context7.next = 8;
            return regeneratorRuntime.awrap(user2.save());

          case 8:
            _context7.next = 13;
            break;

          case 10:
            _context7.prev = 10;
            _context7.t0 = _context7["catch"](1);
            err = _context7.t0;

          case 13:
            expect(err).toBeNull();

          case 14:
          case "end":
            return _context7.stop();
        }
      }
    }, null, null, [[1, 10]]);
  });
  it('ไม่สามารถเพิ่ม ข้อมูล ได้ เพราะ ชื่อ ต้องเป็นตัวอักษรเท่านั้น', function _callee8() {
    var err, user1;
    return regeneratorRuntime.async(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            err = null;
            _context8.prev = 1;
            user1 = new Student(userErrorName);
            _context8.next = 5;
            return regeneratorRuntime.awrap(user1.save());

          case 5:
            _context8.next = 10;
            break;

          case 7:
            _context8.prev = 7;
            _context8.t0 = _context8["catch"](1);
            err = _context8.t0;

          case 10:
            expect(err).toBeNull();

          case 11:
          case "end":
            return _context8.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่ม ข้อมูล ได้ เพราะ gender ไม่ถูกต้อง', function _callee9() {
    var err, user;
    return regeneratorRuntime.async(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            err = null;
            _context9.prev = 1;
            user = new Student(userErrorGender);
            _context9.next = 5;
            return regeneratorRuntime.awrap(user.save());

          case 5:
            _context9.next = 10;
            break;

          case 7:
            _context9.prev = 7;
            _context9.t0 = _context9["catch"](1);
            err = _context9.t0;

          case 10:
            expect(err).not.toBeNull();

          case 11:
          case "end":
            return _context9.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่ม ข้อมูล ได้ เพราะ age ต้องเป็น ตัวเลข', function _callee10() {
    var err, user;
    return regeneratorRuntime.async(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            err = null;
            _context10.prev = 1;
            user = new Student(userErrorAge);
            _context10.next = 5;
            return regeneratorRuntime.awrap(user.save());

          case 5:
            _context10.next = 10;
            break;

          case 7:
            _context10.prev = 7;
            _context10.t0 = _context10["catch"](1);
            err = _context10.t0;

          case 10:
            expect(err).not.toBeNull();

          case 11:
          case "end":
            return _context10.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่ม ข้อมูล ได้ เพราะ tel ต้องไม่เกิน 8 ตัว และ ไม่เกิน 10', function _callee11() {
    var err, user;
    return regeneratorRuntime.async(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            err = null;
            _context11.prev = 1;
            user = new Student(userErrorTel);
            _context11.next = 5;
            return regeneratorRuntime.awrap(user.save());

          case 5:
            _context11.next = 10;
            break;

          case 7:
            _context11.prev = 7;
            _context11.t0 = _context11["catch"](1);
            err = _context11.t0;

          case 10:
            expect(err).not.toBeNull();

          case 11:
          case "end":
            return _context11.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่ม ข้อมูล ได้ เพราะ GPA ต้องไม่มากกว่า 4.00 และ ไม่น้อยกว่า 1.00', function _callee12() {
    var err, user;
    return regeneratorRuntime.async(function _callee12$(_context12) {
      while (1) {
        switch (_context12.prev = _context12.next) {
          case 0:
            err = null;
            _context12.prev = 1;
            user = new Student(userErrorGpa);
            _context12.next = 5;
            return regeneratorRuntime.awrap(user.save());

          case 5:
            _context12.next = 10;
            break;

          case 7:
            _context12.prev = 7;
            _context12.t0 = _context12["catch"](1);
            err = _context12.t0;

          case 10:
            expect(err).not.toBeNull();

          case 11:
          case "end":
            return _context12.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
});
/**
 * Complete product example.
 */

var userComplete = {
  std_id: '60160001',
  name: 'นายสมชาย แมนมาก',
  gender: 'M',
  age: 20,
  email: '60160001@go.buu.ac.th',
  passwd: 'S12345',
  tels: '000000000',
  bdate: '2018-11-10 22:26:12.111Z',
  address: '100/10 ',
  gpa: 3.00,
  schoolyear: 2563
};
var userComplete2 = {
  std_id: '60160001',
  name: 'นางสาวสมหญิง สวยมาก',
  gender: 'M',
  age: 20,
  email: '60160001@go.buu.ac.th',
  passwd: 'S12345',
  tels: '000000000',
  bdate: '2018-11-10 22:26:12.111Z',
  address: '100/10 ',
  gpa: 3.00,
  schoolyear: 2563
};
var userError1 = {
  std_id: '',
  name: '',
  gender: '',
  age: '',
  email: '',
  passwd: '',
  tels: '',
  bdate: '',
  address: '',
  gpa: '',
  schoolyear: ''
};
var userErrorId = {
  std_id: '601600010000',
  name: 'นายสมชาย แมนมาก',
  gender: 'M',
  age: 20,
  email: '60160001@go.buu.ac.th',
  passwd: 'S12345',
  tels: '000000000',
  bdate: '2018-11-10 22:26:12.111Z',
  address: '100/10 ',
  gpa: 3.00,
  schoolyear: 2563
};
var userErrorName = {
  std_id: '60160001',
  name: 2222,
  gender: 'M',
  age: 20,
  email: '60160001@go.buu.ac.th',
  passwd: 'S12345',
  tels: '000000000',
  bdate: '2018-11-10 22:26:12.111Z',
  address: '100/10 ',
  gpa: 3.00,
  schoolyear: 2563
};
var userErrorGender = {
  std_id: '60160001',
  name: 'นางสาวสมหญิง สวยมาก',
  gender: 'A',
  age: 20,
  email: '60160001@go.buu.ac.th',
  passwd: 'S12345',
  tels: '000000000',
  bdate: '2018-11-10 22:26:12.111Z',
  address: '100/10 ',
  gpa: 3.00,
  schoolyear: 2563
};
var userErrorAge = {
  std_id: '60160001',
  name: 'นางสาวสมหญิง สวยมาก',
  gender: 'M',
  age: 'ssada',
  email: '60160001@go.buu.ac.th',
  passwd: 'S12345',
  tels: '000000000',
  bdate: '2018-11-10 22:26:12.111Z',
  address: '100/10 ',
  gpa: 3.00,
  schoolyear: 2563
};
var userErrorTel = {
  std_id: '60160001',
  name: 'นางสาวสมหญิง สวยมาก',
  gender: 'M',
  age: 100,
  email: '60160001@go.buu.ac.th',
  passwd: 'S12345',
  tels: '000000000000',
  bdate: '2018-11-10 22:26:12.111Z',
  address: '100/10 ',
  gpa: 3.00,
  schoolyear: 2563
};
var userErrorGpa = {
  std_id: '60160001',
  name: 'นางสาวสมหญิง สวยมาก',
  gender: 'M',
  age: 100,
  email: '60160001@go.buu.ac.th',
  passwd: 'S12345',
  tels: '000000000000',
  bdate: '2018-11-10 22:26:12.111Z',
  address: '100/10 ',
  gpa: 5.00,
  schoolyear: 2563
};