const dbHandler = require('./db-handler')
const Student = require('../model/Student')

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => {
  await dbHandler.connect()
})

/**
 * Clear all test data after every test.
 */
afterEach(async () => {
  await dbHandler.clearDatabase()
})

/**
 * Remove and close the db and server.
 */
afterAll(async () => {
  await dbHandler.closeDatabase()
})

/**
 * Product test suite.
 */
describe('Student', () => {
  it('สามารถเพิ่ม ข้อมูล ได้', async () => {
    let err = null
    try {
      const user = new Student(userComplete)
      await user.save()
    } catch (e) {
      err = e
    }
    expect(err).toBeNull()
  })
  it('ไม่สามารถเพิ่ม ข้อมูล ได้ เพราะ ห้ามมีช่องไหนเป็น ช่องว่าง', async () => {
    let err = null
    try {
      const user = new Student(userError1)
      await user.save()
    } catch (e) {
      err = e
    }
    expect(err).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม ข้อมูล ได้ เพราะ รหัสนิสิตต้องเท่ากับ 8 ตัว', async () => {
    let err = null
    try {
      const user = new Student(userErrorId)
      await user.save()
    } catch (e) {
      err = e
    }
    expect(err).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม ข้อมูล รหัสนิสิตไม่สามารถซ้ำกันได้', async () => {
    let err = null
    try {
      const user1 = new Student(userComplete)
      await user1.save()
      const user2 = new Student(userComplete2)
      await user2.save()
    } catch (e) {
      err = e
    }
    expect(err).toBeNull()
  })
  it('ไม่สามารถเพิ่ม ข้อมูล ได้ เพราะ ชื่อ ต้องเป็นตัวอักษรเท่านั้น', async () => {
    let err = null
    try {
      const user1 = new Student(userErrorName)
      await user1.save()
    } catch (e) {
      err = e
    }
    expect(err).toBeNull()
  })
  it('ไม่สามารถเพิ่ม ข้อมูล ได้ เพราะ gender ไม่ถูกต้อง', async () => {
    let err = null
    try {
      const user = new Student(userErrorGender)
      await user.save()
    } catch (e) {
      err = e
    }
    expect(err).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม ข้อมูล ได้ เพราะ age ต้องเป็น ตัวเลข', async () => {
    let err = null
    try {
      const user = new Student(userErrorAge)
      await user.save()
    } catch (e) {
      err = e
    }
    expect(err).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม ข้อมูล ได้ เพราะ tel ต้องไม่เกิน 8 ตัว และ ไม่เกิน 10', async () => {
    let err = null
    try {
      const user = new Student(userErrorTel)
      await user.save()
    } catch (e) {
      err = e
    }
    expect(err).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม ข้อมูล ได้ เพราะ GPA ต้องไม่มากกว่า 4.00 และ ไม่น้อยกว่า 1.00', async () => {
    let err = null
    try {
      const user = new Student(userErrorGpa)
      await user.save()
    } catch (e) {
      err = e
    }
    expect(err).not.toBeNull()
  })
})

/**
 * Complete product example.
 */
const userComplete = {
  std_id: '60160001',
  name: 'นายสมชาย แมนมาก',
  gender: 'M',
  age: 20,
  email: '60160001@go.buu.ac.th',
  passwd: 'S12345',
  tels: '000000000',
  bdate: '2018-11-10 22:26:12.111Z',
  address: '100/10 ',
  gpa: 3.00,
  schoolyear: 2563
}

const userComplete2 = {
  std_id: '60160001',
  name: 'นางสาวสมหญิง สวยมาก',
  gender: 'M',
  age: 20,
  email: '60160001@go.buu.ac.th',
  passwd: 'S12345',
  tels: '000000000',
  bdate: '2018-11-10 22:26:12.111Z',
  address: '100/10 ',
  gpa: 3.00,
  schoolyear: 2563
}

const userError1 = {
  std_id: '',
  name: '',
  gender: '',
  age: '',
  email: '',
  passwd: '',
  tels: '',
  bdate: '',
  address: '',
  gpa: '',
  schoolyear: ''
}

const userErrorId = {
  std_id: '601600010000',
  name: 'นายสมชาย แมนมาก',
  gender: 'M',
  age: 20,
  email: '60160001@go.buu.ac.th',
  passwd: 'S12345',
  tels: '000000000',
  bdate: '2018-11-10 22:26:12.111Z',
  address: '100/10 ',
  gpa: 3.00,
  schoolyear: 2563
}

const userErrorName = {
  std_id: '60160001',
  name: 2222,
  gender: 'M',
  age: 20,
  email: '60160001@go.buu.ac.th',
  passwd: 'S12345',
  tels: '000000000',
  bdate: '2018-11-10 22:26:12.111Z',
  address: '100/10 ',
  gpa: 3.00,
  schoolyear: 2563
}

const userErrorGender = {
  std_id: '60160001',
  name: 'นางสาวสมหญิง สวยมาก',
  gender: 'A',
  age: 20,
  email: '60160001@go.buu.ac.th',
  passwd: 'S12345',
  tels: '000000000',
  bdate: '2018-11-10 22:26:12.111Z',
  address: '100/10 ',
  gpa: 3.00,
  schoolyear: 2563
}

const userErrorAge = {
  std_id: '60160001',
  name: 'นางสาวสมหญิง สวยมาก',
  gender: 'M',
  age: 'ssada',
  email: '60160001@go.buu.ac.th',
  passwd: 'S12345',
  tels: '000000000',
  bdate: '2018-11-10 22:26:12.111Z',
  address: '100/10 ',
  gpa: 3.00,
  schoolyear: 2563
}

const userErrorTel = {
  std_id: '60160001',
  name: 'นางสาวสมหญิง สวยมาก',
  gender: 'M',
  age: 100,
  email: '60160001@go.buu.ac.th',
  passwd: 'S12345',
  tels: '000000000000',
  bdate: '2018-11-10 22:26:12.111Z',
  address: '100/10 ',
  gpa: 3.00,
  schoolyear: 2563
}

const userErrorGpa = {
  std_id: '60160001',
  name: 'นางสาวสมหญิง สวยมาก',
  gender: 'M',
  age: 100,
  email: '60160001@go.buu.ac.th',
  passwd: 'S12345',
  tels: '000000000000',
  bdate: '2018-11-10 22:26:12.111Z',
  address: '100/10 ',
  gpa: 5.00,
  schoolyear: 2563
}
