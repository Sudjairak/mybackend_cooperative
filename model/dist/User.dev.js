"use strict";

var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 3,
    unique: true
  },
  gender: {
    type: String,
    "enum": ['M', 'F'],
    required: true
  },
  age: {
    type: Number,
    required: true,
    min: 1,
    max: 99
  },
  email: {
    type: String,
    required: true
  },
  passwd: {
    type: String,
    required: true
  },
  tels: {
    type: String,
    required: true,
    minlength: 9,
    maxlength: 10
  },
  bdate: {
    type: Date,
    required: true
  },
  address: {
    type: String
  }
});
module.exports = mongoose.model('User', userSchema);