"use strict";

var mongoose = require('mongoose');

var studentSchema = new mongoose.Schema({
  type: {
    type: String
  },
  std_id: {
    type: String,
    required: true,
    minlength: 8,
    maxlength: 8
  },
  name: {
    type: String,
    required: true,
    minlength: 3,
    unique: true
  },
  gender: {
    type: String,
    "enum": ['M', 'F'],
    required: true
  },
  age: {
    type: Number,
    required: true,
    min: 1,
    max: 99
  },
  email: {
    type: String,
    required: true
  },
  passwd: {
    type: String,
    required: true
  },
  tels: {
    type: String,
    required: true,
    minlength: 9,
    maxlength: 10
  },
  bdate: {
    type: Date,
    required: true
  },
  address: {
    type: String
  },
  gpa: {
    type: Number,
    required: true,
    min: 0.01,
    max: 4.00
  },
  schoolyear: {
    type: Number,
    "enum": [2563, 2564],
    required: true
  },
  status: {
    property_sts: {
      type: String
    },
    doc_sts: {
      type: String
    },
    company_sts: {
      type: String
    },
    interv_sts: {
      type: String
    },
    coop_sts: {
      type: String
    }
  },
  training: {
    prepare: {
      type: Number
    },
    academic: {
      type: Number
    }
  },
  subjects: [{
    id: {
      type: String
    },
    name: {
      type: String
    },
    grade: {
      type: Number
    }
  }, {
    id: {
      type: String
    },
    name: {
      type: String
    },
    grade: {
      type: Number
    }
  }, {
    id: {
      type: String
    },
    name: {
      type: String
    },
    grade: {
      type: Number
    }
  }, {
    id: {
      type: String
    },
    name: {
      type: String
    },
    grade: {
      type: Number
    }
  }, {
    id: {
      type: String
    },
    name: {
      type: String
    },
    grade: {
      type: Number
    }
  }, {
    id: {
      type: String
    },
    name: {
      type: String
    },
    grade: {
      type: Number
    }
  }, {
    id: {
      type: String
    },
    name: {
      type: String
    },
    grade: {
      type: Number
    }
  }, {
    id: {
      type: String
    },
    name: {
      type: String
    },
    grade: {
      type: Number
    }
  }, {
    id: {
      type: String
    },
    name: {
      type: String
    },
    grade: {
      type: Number
    }
  }, {
    id: {
      type: String
    },
    name: {
      type: String
    },
    grade: {
      type: Number
    }
  }]
});
module.exports = mongoose.model('Student', studentSchema);